        <script src="site/js/jquery.min.js<?= VERSION; ?>"></script>
        <script src="site/js/jquery-migrate-3.0.1.min.js<?= VERSION; ?>"></script>
        <script src="site/js/popper.min.js<?= VERSION; ?>"></script>
        <script src="site/js/bootstrap.min.js<?= VERSION; ?>"></script>
        <script src="site/js/jquery.easing.1.3.js<?= VERSION; ?>"></script>
        <script src="site/js/jquery.waypoints.min.js<?= VERSION; ?>"></script>
        <script src="site/js/jquery.stellar.min.js<?= VERSION; ?>"></script>
        <script src="site/js/owl.carousel.min.js<?= VERSION; ?>"></script>
        <script src="site/js/jquery.magnific-popup.min.js<?= VERSION; ?>"></script>
        <script src="site/js/aos.js<?= VERSION; ?>"></script>
        <script src="site/js/jquery.animateNumber.min.js<?= VERSION; ?>"></script>
        <script src="site/js/bootstrap-datepicker.js<?= VERSION; ?>"></script>
        <!--<script src="site/js/jquery.timepicker.min.js<?= VERSION; ?>"></script>-->
        <script src="site/js/scrollax.min.js<?= VERSION; ?>"></script>
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>-->
        <!--<script src="site/js/google-map.js<?= VERSION; ?>"></script>-->
        <script src="<?php echo ASSETS; ?>vendor/alertify/js/alertify.js"></script>
        <script src="site/js/main.js<?= VERSION; ?>"></script>
        <script src="<?php echo ASSETS; ?>lte-admin/js/common.js<?= VERSION; ?>"></script>
        <script src="<?php echo ASSETS; ?>js/main.js<?= VERSION; ?>"></script>

        <!--
        Developed By Revo Interactive
        -->
        
    </body>
</html>