<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="<?php echo BASE_URL; ?>">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   

        <meta name="generator" content="Mawa Framework 1.0.2" />
        <meta name="format-detection" content="telephone=yes">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="robots" content="index, follow" />

        <title><?php echo $this->settings->title; ?></title>
        <meta name="keywords" content="<?php echo $this->settings->keyword; ?>" />
        <meta name="description" content="<?php echo $this->settings->description; ?>" />
        <meta name="author" content="<?php echo $this->settings->company; ?>" />
        <meta name="copyright" content="<?php echo $this->settings->company; ?>" />
        
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700" rel="stylesheet">

        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="site/images/apple-touch-icon.png">

        <link rel="stylesheet" href="asset/css/open-iconic-bootstrap.min.css<?= VERSION; ?>">
        <link rel="stylesheet" href="site/css/animate.css<?= VERSION; ?>">

        <link rel="stylesheet" href="<?= BASE_URL; ?>assets/css/owl.carousel.min.css<?= VERSION; ?>">
        <link rel="stylesheet" href="site/css/owl.theme.default.min.css<?= VERSION; ?>">
        <link rel="stylesheet" href="site/css/magnific-popup.css<?= VERSION; ?>">

        <link rel="stylesheet" href="site/css/aos.css<?= VERSION; ?>">

        <link rel="stylesheet" href="site/css/ionicons.min.css<?= VERSION; ?>">

        <link rel="stylesheet" href="site/css/bootstrap-datepicker.css<?= VERSION; ?>">
        <link rel="stylesheet" href="site/css/jquery.timepicker.css<?= VERSION; ?>">

        <link rel="stylesheet" href="<?php echo ASSETS; ?>vendor/alertify/css/alertify.min.css" />
        <link rel="stylesheet" href="<?php echo ASSETS; ?>vendor/alertify/css/themes/default.min.css" />

        <link rel="stylesheet" href="site/css/flaticon.css<?= VERSION; ?>">
        <link rel="stylesheet" href="site/css/font-awesome-4.7.0/css/font-awesome.min.css<?= VERSION; ?>">
        <link rel="stylesheet" href="site/css/icomoon.css<?= VERSION; ?>">
        <link rel="stylesheet" href="<?php echo ASSETS; ?>lte-admin/css/custom.css<?= VERSION; ?>">
        <link rel="stylesheet" href="site/css/style.css<?= VERSION; ?>">
        
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>
            var BASE_URL = '<?php echo BASE_URL; ?>';
        </script>
    </head>